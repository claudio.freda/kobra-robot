#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/PoseStamped.h"
#include <math.h>
#include "tf/transform_datatypes.h"
#include <tf/transform_listener.h> 

#define RUN_PERIOD_DEFAULT 0.1
#define NAME_OF_THIS_NODE "odom_node"

enum IntegrationMode
{
	EULER,
	RUNGE_KUTTA,
	EXACT,
	N_INTEGRATION_MODE
};

class ROSnode {
private: 
    ros::NodeHandle Handle;
    ros::Subscriber odomSub;
    ros::Publisher posePub;
    ros::Publisher odomPub;
    double x, y, yaw, t;
    int integrationMode;
    
    void odomCallback(const nav_msgs::Odometry::ConstPtr& msg);

public:
    bool Prepare();
    void RunContinuously();
    void Shutdown();
};

//-----------------------------------------------------------------
//-----------------------------------------------------------------

bool ROSnode::Prepare() {
    ROS_INFO("%s", ros::this_node::getName().c_str());

    if (!Handle.getParam(ros::this_node::getName()+"/integration_mode", integrationMode) || integrationMode < 0 || integrationMode >= N_INTEGRATION_MODE)
    {
		ROS_WARN("Unspecified integration mode. Using RUNGE_KUTTA. (euler = 0, runge-kutta = 1, exact = 2)");
		integrationMode = RUNGE_KUTTA;
	}
	/*
	tf::TransformListener listener;
	tf::StampedTransform transform;
	try {
		listener.waitForTransform("/world", "/robot_gt", ros::Time(0),	ros::Duration(2.0));
		listener.lookupTransform("/world",	"/robot_gt", ros::Time(0), transform);
	} catch (tf::TransformException ex) { ROS_ERROR("%s",ex.what()); }
	
	x = transform.getOrigin().getX();
	y = transform.getOrigin().getY();
	
	tf::Matrix3x3 m(transform.getRotation());
    double roll, pitch;
    m.getRPY(roll, pitch, yaw);*/
	
	x = 0;
	y = 0;
	yaw = 0;
	
    odomSub = Handle.subscribe("/robot/odometry", 10, &ROSnode::odomCallback, this);
    posePub = Handle.advertise<geometry_msgs::PoseStamped>("/robot/pose", 10);
    odomPub = Handle.advertise<nav_msgs::Odometry>("/robot/odometry2", 10);
    ROS_INFO("Node %s ready to run.", ros::this_node::getName().c_str());
    return true;
}


void ROSnode::RunContinuously() {
  ROS_INFO("Node %s running continuously.", ros::this_node::getName().c_str());
   
  ros::spin();
}

void ROSnode::Shutdown() {
  ROS_INFO("Node %s shutting down.", ros::this_node::getName().c_str());
}


void ROSnode::odomCallback(const nav_msgs::Odometry::ConstPtr& msg) {
    if(t < 0) {
        t = msg->header.stamp.toSec();
        return;
    }
    float v = msg->twist.twist.linear.x;
    float w = msg->twist.twist.angular.z;
    double dt = msg->header.stamp.toSec() - t;
    
    double yaw2 = yaw + w*dt;
    
    switch (integrationMode)
    {
	case EULER:
		x = x + v*cos(yaw)*dt;
		y = y + v*sin(yaw)*dt;
		yaw = yaw2;
		break;
	case RUNGE_KUTTA:
		x = x + v*cos(yaw + w*dt*0.5)*dt;
		y = y + v*sin(yaw + w*dt*0.5)*dt;
		yaw = yaw2;
		break;
	case EXACT:
	default:
   		x = x + v/w*(sin(yaw2) - sin(yaw));
		y = y + v/w*(cos(yaw2) - cos(yaw));
		yaw = yaw2;
		break;
	}
    geometry_msgs::PoseStamped out;
    out.header = msg->header;
    out.header.frame_id = "/world";
    out.pose.orientation = tf::createQuaternionMsgFromYaw(yaw);
    out.pose.position.x = x;
    out.pose.position.y = y;
    out.pose.position.z = 0.13;    
    posePub.publish(out);
    
    nav_msgs::Odometry odom = *msg;
    odom.header.frame_id = "/world";
    odom.pose.pose = out.pose;
    odomPub.publish(odom);
    
    t = msg->header.stamp.toSec();
}

int main(int argc, char **argv) {
  ros::init(argc, argv, NAME_OF_THIS_NODE);
  
  ROSnode mNode;
   
  if(!mNode.Prepare()) return (-1);
  mNode.RunContinuously();
  mNode.Shutdown();
  
  return (0);
}
